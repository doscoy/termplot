#include <cmath>
#include <iostream>
#include <random>
#include <thread>

#include <termplot.hpp>

int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv) {
  {
    std::cout << "Canvas\n";

    termplot::Canvas canvas{40, 20};
    struct p {
      int x, y;
    };
    const p p0{0, canvas.GetHeight() - 1};
    const p p1{canvas.GetWidth() - 2, canvas.GetHeight() - 1};
    const p c{p1.x / 2, 0};

    canvas.DrawLine(p0.x, p0.y, c.x, c.y);
    canvas.DrawLine(p1.x, p1.y, c.x, c.y);
    canvas.DrawLine(p0.x, p0.y, p1.x, p1.y, c.x, c.y);

    std::cout << canvas << '\n';
  }

  {
    std::cout << "Plotter\n";

    termplot::Plotter plotter{-1.0, 1.0, -1.0, 1.0, 40, 20};

    std::random_device seed_gen;
    std::mt19937 engine{seed_gen()};
    std::uniform_real_distribution<> dist{-1.0, 1.0};

    for (int i = 0; i < 1000; ++i) {
      plotter.Plot(dist(engine), dist(engine));
    }

    std::cout << plotter << '\n';
  }

  {
    std::cout << "VBar\n";

    termplot::VBar vbar{-1.0, 1.0, 40, 1, 20};

    for (int i = 0; i < vbar.GetSeriesLen(); ++i) {
      const auto n{i * 2 / static_cast<double>(vbar.GetSeriesLen() - 1) - 1.0};
      vbar.Plot(i, n);
    }

    std::cout << vbar << '\n';
  }

  {
    std::cout << "HBar\n";

    termplot::HBar hbar{-1.0, 1.0, 40, 1, 40};

    for (int i = 0; i < hbar.GetSeriesLen(); ++i) {
      const auto n{i * 2 / static_cast<double>(hbar.GetSeriesLen() - 1) - 1.0};
      hbar.Plot(i, n);
    }

    std::cout << hbar << '\n';
  }

  {
    std::cout << "VPushBar\n";

    termplot::VPushBar vpbar{-1.0, 1.0, 40, 20};

    for (int deg = 0; deg < 360; ++deg) {
      using clock = std::chrono::high_resolution_clock;
      const auto next{clock::now() + std::chrono::milliseconds(50)};

      vpbar.Push(std::sin(deg * 5 * (3.1415926535 / 180.0)));
      std::cout << vpbar << "\033[22F";

      std::this_thread::sleep_until(next);
    }

    std::cout << vpbar << '\n';
  }

  {
    std::cout << "HPushBar\n";

    termplot::HPushBar hpbar{-1.0, 1.0, 40, 20};

    for (int deg = 0; deg < 360; ++deg) {
      using clock = std::chrono::high_resolution_clock;
      const auto next{clock::now() + std::chrono::milliseconds(50)};

      hpbar.Push(std::sin(deg * 5 * (3.1415926535 / 180.0)));
      std::cout << hpbar << "\033[23F";

      std::this_thread::sleep_until(next);
    }

    std::cout << hpbar << '\n';
  }
}
# termplot

簡易的なグラフを標準出力等に出力する

## 使い方

### Canvas

```cpp
// サイズを文字数で指定する (40文字20行 -> 80x80ドット)
termplot::Canvas canvas{40, 20};

// 整数でxy座標を入力してドットを打つ
canvas.Plot(40, 20);

// 整数で始点と終点のxy座標を入力して直線を引く
canvas.DrawLine( 0,  0, 79, 79);
//              ~~~~~~  ~~~~~~
//              ^始点　　^終点

// 整数で始点と終点とコントロールポイントのxy座標を入力してベジェ曲線を引く
canvas.DrawLine( 0,  0, 79,  0, 40, 79);
//              ~~~~~~  ~~~~~~  ~~~~~~
//              ^始点　　^終点    ^コントロールポイント

// ostreamに出力
std::cout << canvas;

// 文字列として取得
std::string str = canvas.GetString();
```
Canvasの原点は左上となる

![canvas](./img/canvas.png)

---

### Plotter

```cpp
// 範囲を実数で、サイズを文字数で指定する (-1.0 <= x <= 1.0, -1.0 <= y <= 1.0, 40文字20行)
termplot::Plotter plotter{-1.0, 1.0, -1.0, 1.0, 40, 20};

// 実数で入力する以外はCanvasと同じ
plotter.Plot(0.0, -0.5);
plotter.DrawLine(-1.0, -1.0, 1.0, 1.0);
plotter.DrawLine(-1.0, -1.0, 1.0, -1.0, 0.0, 1.0);

// ostreamに出力
std::cout << plotter;
```
Plotterの原点は左下となる

![plotter](./img/plotter.png)

---

### VBar, Hbar

```cpp
// 範囲を実数で、系列数と間隔、行数を整数で指定する (-1.0 <= n <= 1.0, 9系列, 間隔3ドット, 6行)
termplot::VBar vbar{-1.0, 1.0, 9, 3, 6};

// 範囲を実数で、系列数と間隔、行毎の文字数を整数で指定する (-1.0 <= n <= 1.0, 9系列, 間隔3ドット, 6文字)
termplot::HBar hbar{-1.0, 1.0, 9, 3, 12};


constexpr double kData[9]{-1.0, -0.75, -0.5, -0.25, 0.0, 0.25, 0.5, 0.75, 1.0};
for (int i = 0; i < 9; ++i) {
  // インデックスで系列を指定して値を入力する
  vbar.Plot(i, kData[i]);
  hbar.Plot(i, kData[i]);
}

// ostreamに出力
std::cout << "VBar\n" << vbar;
std::cout << "HBar\n" << hbar;
```
![vbar](./img/vbar.png)![hbar](./img/hbar.png)

---

### VPushBar, HPushbar

```cpp
// 範囲を実数で、サイズを文字数で指定する
termplot::VPushBar vpushbar{-1.0, 1.0, 40, 10};
termplot::HPushBar hpushbar{-1.0, 1.0, 20, 20};

for (int deg = 0; deg < 360; ++deg) {
  const auto sin{std::sin(deg * 5 * (3.1415926535 / 180.0))};

  // 値を入力する
  vpushbar.Push(sin);
  hpushbar.Push(sin);

  // ostreamに出力
  std::cout << "VPushBar\n" << vpushbar;
  std::cout << "HPushBar\n" << hpushbar;
}
```
![vbar](./img/vpushbar.gif)![hbar](./img/hpushbar.gif)

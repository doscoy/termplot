#include <cmath>
#include <cstdio>
#include <termplot.hpp>

namespace termplot {

Canvas::Canvas(int columns, int rows) : columns_{columns}, rows_{rows} {
  assert((rows_ > 0) && (columns_ > 0));
  buffer_.resize(rows_ * columns_);
}

namespace {

auto inline constexpr IsWithin(int beg, int end, int n) noexcept -> bool {
  return (beg <= n && n < end);
}

}  // namespace

auto Canvas::GetDot(int x, int y) const noexcept -> Dot {
  if (not IsWithin(0, GetWidth(), x)) return Dot{-1, 0};
  if (not IsWithin(0, GetHeight(), y)) return Dot{-1, 0};

  constexpr std::uint8_t kTable[4][2]{{0b00000001, 0b00001000},   // ⠁, ⠈
                                      {0b00000010, 0b00010000},   // ⠂, ⠐
                                      {0b00000100, 0b00100000},   // ⠄, ⠠
                                      {0b01000000, 0b10000000}};  // ⡀, ⢀

  return Dot{(x >> 1) + (columns_ * (y >> 2)), kTable[y & 0b11][x & 0b1]};
}

void Canvas::Plot(int x, int y) noexcept {
  const auto dot{GetDot(x, y)};
  if (not IsWithin(0, buffer_.size(), dot.index)) return;

  buffer_[dot.index] |= dot.mask;
}

void Canvas::DrawLine(int x0, int y0, int x1, int y1) {
  // https://ja.wikipedia.org/wiki/%E3%83%96%E3%83%AC%E3%82%BC%E3%83%B3%E3%83%8F%E3%83%A0%E3%81%AE%E3%82%A2%E3%83%AB%E3%82%B4%E3%83%AA%E3%82%BA%E3%83%A0
  const auto dx{std::abs(x1 - x0)};
  const auto dy{std::abs(y1 - y0)};
  const auto sx{x0 < x1 ? 1 : -1};
  const auto sy{y0 < y1 ? 1 : -1};

  auto err{dx - dy};
  while (true) {
    Plot(x0, y0);
    if (x0 == x1 && y0 == y1) break;
    const auto e2{err * 2};
    if (e2 > -dy) {
      err += -dy;
      x0 += sx;
    }
    if (e2 < dx) {
      err += dx;
      y0 += sy;
    }
  }
}

void Canvas::DrawLine(int x0, int y0, int x1, int y1, int cx, int cy) {
  // https://ja.javascript.info/bezier-curve
  constexpr auto bezier{[](auto p0, auto p1, auto c, auto t) -> int {
    const auto t_sub{1.0 - t};
    return std::round(t_sub * t_sub * p0 + 2 * t_sub * t * c + t * t * p1);
  }};

  const auto len{
      std::round(std::sqrt((cx - x0) * (cx - x0) + (cy - y0) * (cy - y0)) +
                 std::sqrt((cx - x1) * (cx - x1) + (cx - y1) * (cx - y1)))};

  for (double t = 0; t <= len; ++t)
    Plot(bezier(x0, x1, cx, t / len), bezier(y0, y1, cy, t / len));
}

void Canvas::Erase(int x, int y) noexcept {
  const auto dot{GetDot(x, y)};
  if (not IsWithin(0, buffer_.size(), dot.index)) return;

  buffer_[dot.index] &= ~dot.mask;
}

void Canvas::EraseRow(int line, std::uint8_t mask) noexcept {
  if (not IsWithin(0, rows_, line)) return;

  const auto head{columns_ * line};
  const auto tail{head + columns_};
  for (int i = head; i < tail; ++i) buffer_[i] &= ~mask;
}

void Canvas::EraseColumn(int column, std::uint8_t mask) noexcept {
  if (not IsWithin(0, columns_, column)) return;

  const auto head{column};
  const auto tail{rows_ * (columns_ - 1) + head};
  for (int i = head; i < tail; i += columns_) buffer_[i] &= ~mask;
}

namespace {

auto GenSubStr(double n) -> std::string {
  char buf[16];
  const auto len{std::snprintf(buf, 16, "%g", n)};
  return std::string(buf, len);
}

auto constexpr FixOffset(int offset, int range) -> int {
  return offset >= 0 ? offset % range : range - (std::abs(offset) % range);
}

}  // namespace

auto Canvas::GetString(double xmin_sub, double xmax_sub, double ymin_sub,
                       double ymax_sub, int col_offset, int row_offset) const
    -> std::string {
  const auto need_ysub{ymin_sub != ymax_sub};
  const auto ymin_str{need_ysub ? GenSubStr(ymin_sub) : std::string{}};
  const auto ymax_str{need_ysub ? GenSubStr(ymax_sub) : std::string{}};

  std::string buf{};
  buf.reserve(((columns_ + 2) * (rows_ + 2) * 3) + (columns_ + 2) +
              (rows_ + 3) + ymin_str.size() + ymax_str.size());

  buf.push_back(' ');
  for (int i = 0; i < columns_; ++i) buf.append("▁");
  buf.push_back(' ');
  buf.append(ymin_str);
  buf.push_back('\n');

  col_offset = FixOffset(col_offset, columns_);
  row_offset = FixOffset(row_offset, rows_);

  for (int y = 0; y < rows_; ++y) {
    buf.append("▕");
    for (int x = 0; x < columns_; ++x) {
      const auto fix_x{(x + col_offset) % columns_};
      const auto fix_y{(y + row_offset) % rows_};
      const auto index{fix_x + (columns_ * fix_y)};
      if (buffer_[index] == 0) {
        buf.push_back(' ');
        // buf.append("\u2800");
      } else {
        const auto second{(buffer_[index] >> 6) & 0x03};
        const auto third{buffer_[index] & 0x3f};
        const char str[3]{'\xE2', static_cast<char>('\xA0' + second),
                          static_cast<char>('\x80' + third)};
        buf.append(std::begin(str), std::end(str));
      }
    }
    buf.append("▏\n");
  }

  buf.push_back(' ');
  for (int i = 0; i < columns_; ++i) buf.append("▔");
  buf.push_back(' ');
  buf.append(ymax_str);
  buf.push_back('\n');

  const auto need_xsub{xmin_sub != xmax_sub};
  if (need_xsub) {
    const auto xmin_str{GenSubStr(xmin_sub)};
    const auto xmax_str{GenSubStr(xmax_sub)};
    buf.append(xmin_str);
    const auto space{
        std::fdim(columns_ + 2 - xmin_str.size(), xmax_str.size())};
    for (int i = 0; i < space; ++i) buf.push_back(' ');
    buf.append(xmax_str);
    buf.push_back('\n');
  }

  return buf;
}

namespace {

auto constexpr ToCanvasPos(int canvas_size, double min, double max, double pos)
    -> int {
  return std::round((pos - min) / (max - min) * (canvas_size - 1));
}

auto constexpr ToFlipCanvasPos(int canvas_size, double min, double max,
                               double pos) -> int {
  return std::round((canvas_size - 1) -
                    ((pos - min) / (max - min) * (canvas_size - 1)));
}

}  // namespace

Plotter::Plotter() : Plotter{0, 1, 0, 1, 40, 20} {}

Plotter::Plotter(double x_min, double x_max, double y_min, double y_max,
                 int columns, int rows)
    : canvas_{columns, rows},
      x_min_{x_min},
      x_max_{x_max},
      y_min_{y_min},
      y_max_{y_max} {}

void Plotter::Plot(double x, double y) {
  const auto w{canvas_.GetWidth()};
  const auto h{canvas_.GetHeight()};
  canvas_.Plot(ToCanvasPos(w, x_min_, x_max_, x),
               ToFlipCanvasPos(h, y_min_, y_max_, y));
}

void Plotter::DrawLine(double x0, double y0, double x1, double y1) {
  const auto w{canvas_.GetWidth()};
  const auto h{canvas_.GetHeight()};
  canvas_.DrawLine(ToCanvasPos(w, x_min_, x_max_, x0),
                   ToFlipCanvasPos(h, y_min_, y_max_, y0),
                   ToCanvasPos(w, x_min_, x_max_, x1),
                   ToFlipCanvasPos(h, y_min_, y_max_, y1));
}

void Plotter::DrawLine(double x0, double y0, double x1, double y1, double cx,
                       double cy) {
  const auto w{canvas_.GetWidth()};
  const auto h{canvas_.GetHeight()};
  canvas_.DrawLine(ToCanvasPos(w, x_min_, x_max_, x0),
                   ToFlipCanvasPos(h, y_min_, y_max_, y0),
                   ToCanvasPos(w, x_min_, x_max_, x1),
                   ToFlipCanvasPos(h, y_min_, y_max_, y1),
                   ToCanvasPos(w, x_min_, x_max_, cx),
                   ToFlipCanvasPos(h, y_min_, y_max_, cy));
}

namespace {

auto constexpr CalcDots(int num_of_series, int space) -> int {
  return (space + 1) * num_of_series - space;
}

auto constexpr CalcCanvasCols(int num_of_series, int space) -> int {
  const auto dots{CalcDots(num_of_series, space)};
  return (dots / 2) + (dots % 2);
}

auto constexpr CalcCanvasLines(int num_of_series, int space) -> int {
  const auto dots{CalcDots(num_of_series, space)};
  return (dots / 4) + std::clamp(dots % 4, 0, 1);
}

auto constexpr CalcOffset(int height, int num_of_series, int space) -> int {
  const auto dots{CalcDots(num_of_series, space)};
  return (height - dots) / 2;
}

}  // namespace

VBar::VBar(int series_len, int space) : VBar{0, 1, series_len, space, 10} {}

VBar::VBar(double min, double max, int series_len, int space, int rows)
    : canvas_{CalcCanvasCols(series_len, space), rows},
      min_{min},
      max_{max},
      series_len_{series_len},
      space_{space} {}

void VBar::Plot(int series_idx, double n) {
  const auto h{canvas_.GetHeight()};
  const auto x{series_idx * (space_ + 1)};
  canvas_.DrawLine(x, ToFlipCanvasPos(h, min_, max_, 0), x,
                   ToFlipCanvasPos(h, min_, max_, n));
}

HBar::HBar(int series_len, int space) : HBar{0, 1, series_len, space, 40} {}

HBar::HBar(double min, double max, int series_len, int space, int columns)
    : canvas_{columns, CalcCanvasLines(series_len, space)},
      min_{min},
      max_{max},
      series_len_{series_len},
      space_{space},
      offset_{CalcOffset(canvas_.GetHeight(), series_len, space)} {}

void HBar::Plot(int series_idx, double n) {
  const auto w{canvas_.GetWidth()};
  const auto y{((space_ + 1) * series_len_ - space_ - 1) -
               series_idx * (space_ + 1) + offset_};
  canvas_.DrawLine(ToCanvasPos(w, min_, max_, 0), y,
                   ToCanvasPos(w, min_, max_, n), y);
}

VPushBar::VPushBar() : VPushBar{0, 1, 40, 10} {}

VPushBar::VPushBar(double min, double max, int columns, int rows)
    : canvas_{columns, rows}, min_{min}, max_{max}, counter_{0} {}

void VPushBar::Push(double n) {
  const auto w{canvas_.GetWidth()};
  const auto h{canvas_.GetHeight()};
  const auto x{(counter_ + w - 1) % w};

  if ((x & 0b1) == 0) canvas_.EraseColumn(x >> 1);
  canvas_.DrawLine(x, ToFlipCanvasPos(h, min_, max_, 0), x,
                   ToFlipCanvasPos(h, min_, max_, n));

  counter_ = (counter_ + 1) % w;
}

HPushBar::HPushBar() : HPushBar{0, 1, 40, 10} {}

HPushBar::HPushBar(double min, double max, int columns, int rows)
    : canvas_{columns, rows}, min_{min}, max_{max}, counter_{0} {}

void HPushBar::Push(double n) {
  const auto w{canvas_.GetWidth()};
  const auto h{canvas_.GetHeight()};

  counter_ = (counter_ + 1) % h;

  const auto y{h - counter_ - 1};

  if ((y & 0b11) == 0b11) canvas_.EraseRow(y >> 2);
  canvas_.DrawLine(ToCanvasPos(w, min_, max_, 0), y,
                   ToCanvasPos(w, min_, max_, n), y);
}

}  // namespace termplot
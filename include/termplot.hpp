#pragma once

#include <iostream>
#include <string>
#include <vector>

namespace termplot {

namespace detail {

template <class T>
class Printer {
  friend auto operator<<(std::ostream& os, const Printer& obj)
      -> std::ostream& {
    const auto str{static_cast<const T&>(obj).GetString()};
    os.write(str.data(), str.size());
    return os;
  }
};

}  // namespace detail

class Canvas : public detail::Printer<Canvas> {
 public:
  Canvas(int columns, int rows);

  auto GetRows() const noexcept -> int { return rows_; }
  auto GetColumns() const noexcept -> int { return columns_; }
  auto GetHeight() const noexcept -> int { return rows_ * 4; }
  auto GetWidth() const noexcept -> int { return columns_ * 2; }
  void Clear() { std::fill(buffer_.begin(), buffer_.end(), 0); }

  void Plot(int x, int y) noexcept;
  void DrawLine(int x0, int y0, int x1, int y1);
  void DrawLine(int x0, int y0, int x1, int y1, int cx, int cy);

  void Erase(int x, int y) noexcept;
  void EraseRow(int line, std::uint8_t mask = 0b11111111) noexcept;
  void EraseColumn(int column, std::uint8_t mask = 0b11111111) noexcept;

  auto GetString(double xmin_sub, double xmax_sub, double ymin_sub,
                 double ymax_sub, int col_offset, int row_offset) const
      -> std::string;
  auto GetString() const -> std::string { return GetString(0, 0, 0, 0, 0, 0); }

 private:
  struct Dot {
    int index;
    std::uint8_t mask;
  };

  static auto CalcOffset(int offset, int range) -> int;
  auto GetDot(int x, int y) const noexcept -> Dot;

  int columns_;
  int rows_;
  std::vector<std::uint8_t> buffer_;
};

class Plotter : public detail::Printer<Plotter> {
 public:
  Plotter();
  Plotter(double x_min, double x_max, double y_min, double y_max, int columns,
          int rows);

  auto GetXMin() const noexcept -> double { return x_min_; }
  auto GetXMax() const noexcept -> double { return x_max_; }
  auto GetYMin() const noexcept -> double { return y_min_; }
  auto GetYMax() const noexcept -> double { return y_max_; }

  void Plot(double x, double y);
  void DrawLine(double x0, double y0, double x1, double y1);
  void DrawLine(double x0, double y0, double x1, double y1, double cx,
                double cy);

  void Clear() { canvas_.Clear(); }
  auto GetString() const -> std::string {
    return canvas_.GetString(x_min_, x_max_, y_max_, y_min_, 0, 0);
  }

 private:
  Canvas canvas_;
  double x_min_;
  double x_max_;
  double y_min_;
  double y_max_;
};

class VBar : public detail::Printer<VBar> {
 public:
  VBar(int series_len, int space);
  VBar(double min, double max, int series_len, int space, int rows);

  auto GetSeriesLen() const noexcept -> int { return series_len_; }

  void Plot(int series_idx, double n);

  void Clear() { canvas_.Clear(); }
  auto GetString() const -> std::string {
    return canvas_.GetString(0, series_len_ - 1, max_, min_, 0, 0);
  }

 private:
  Canvas canvas_;
  double min_;
  double max_;
  int series_len_;
  int space_;
};

class HBar : public detail::Printer<HBar> {
 public:
  HBar(int series_len, int space);
  HBar(double min, double max, int series_len, int space, int columns);

  auto GetSeriesLen() const noexcept -> int { return series_len_; }

  void Plot(int series_idx, double n);

  void Clear() { canvas_.Clear(); }
  auto GetString() const -> std::string {
    return canvas_.GetString(min_, max_, series_len_ - 1, 0, 0, 0);
  }

 private:
  Canvas canvas_;
  double min_;
  double max_;
  int series_len_;
  int space_;
  int offset_;
};

class VPushBar : public detail::Printer<VPushBar> {
 public:
  VPushBar();
  VPushBar(double min, double max, int columns, int rows);

  void Push(double n);

  void Clear() { canvas_.Clear(); }
  auto GetString() const -> std::string {
    return canvas_.GetString(0, 0, max_, min_, counter_ >> 1, 0);
  }

 private:
  Canvas canvas_;
  double min_;
  double max_;
  int counter_;
};

class HPushBar : public detail::Printer<HPushBar> {
 public:
  HPushBar();
  HPushBar(double min, double max, int columns, int rows);

  void Push(double n);

  void Clear() { canvas_.Clear(); }
  auto GetString() const -> std::string {
    return canvas_.GetString(min_, max_, 0, 0, 0, -((counter_ >> 2) + 1));
  }

 private:
  Canvas canvas_;
  double min_;
  double max_;
  int counter_;
};

}  // namespace termplot